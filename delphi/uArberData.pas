unit uArberData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, Math, StdCtrls, Types, ComCtrls, RegularExpressions,
  Generics.Collections, Generics.Defaults, Contnrs, uArberAccountData, DateUtils;

type
  TArberEvent = class
  public

    function    CheckValidAcc(const aList : TStringList): boolean;
    function    Check2017Acc(const aList : TStringList): boolean;
    function    CheckValidBet(const aList : TStringList): boolean;
    function    IsBet(const aList : TStringList): boolean;
    function    IsWeb(const aList : TStringList): boolean;
    function    IsNotLive(const aList : TStringList): boolean;
    function    IsSingleBet(const aList : TStringList): boolean;
    function    IsDeposit(const aList : TStringList): boolean;
    function    IsDepositIncrease(const aList : TStringList): boolean;
    function    IsFromCard(const aList : TStringList): boolean;
    function    GetFileNames(const aPath : string): TStringList;
    function    GetData(const aPath : string; aFileNamesList : TStringList): TObjectList;
    function    GetArberList(const aStr : string): TStringList;
    function    PutfX2List(const aFilePath : String): TObjectList;
    function    getDayOfYear(const str : string): integer;
    procedure   PutArberOrNot(const aFilePath : String; tList : tObjectList; const v1,v2,v3 : double);
    function    CountfX5(const aFilePath : String): double;
    function    CountFX6(const aFilePath : String): integer;
    function    CountFX9(const aFilePath : String): double;
    function    CountfX7(const aFilePath : String): double;
    function    CountfX8(const aFilePath : String): double;
    function    GetDep(const aFilePath : String): integer;
    function    GetBets(const aFilePath : String): integer;
    procedure   CountAndWritefX2(const aArberAllAccsList : TObjectList);
  end;

implementation

var
  OUTCOME_NOTATION, DIMENSION: integer;


function TArberEvent.GetFileNames(const aPath : string): TStringList;
var
  sr    : TSearchRec;
  attr  : Integer;
  path  : String;
begin
  path := IncludeTrailingPathDelimiter(aPath);
  attr := faAnyFile - faVolumeID - faDirectory; 
  result := TStringList.Create;
  if FindFirst(path + '*', attr, sr) = 0 then
    begin
      repeat
        result.add(sr.name);
      until
        FindNext(sr) <> 0;
      FindClose(sr);
    end;
end;

function TArberEvent.GetArberList(const aStr : string): TStringList;
var
  i : integer;
begin
  result := TStringList.Create;
  result.Delimiter := ',';
  result.StrictDelimiter := True;
  result.DelimitedText := aStr;
end;

function TArberEvent.CheckValidBet(const aList : TStringList): boolean;
begin
  result := true;
  if aList[6] <> chr(39) + 'PAYOUT' + chr(39) then
    begin
      result := false;
      exit;
  end;
  if aList[8] <> '0' then
    begin
      result := false;
      exit;
  end;
end;

function TArberEvent.IsSingleBet(const aList : TStringList): boolean;
begin
  result := false;
  if aList.Count < 9 then
    begin
    result := false;
    exit;
  end;
  if aList[8] = chr(39) + 'SINGLE' + chr(39) then
        result := true;
end;

function TArberEvent.CheckValidAcc(const aList : TStringList): boolean;
begin
  result := true;
  if (aList[5] <> chr(39) + 'finished' + chr(39)) and (Copy(aList[6], 5, 3) = 'Win') then
    begin
      result := false;
  end;
end;

function TArberEvent.IsBet(const aList : TStringList): boolean;
begin
  result := true;
  if aList[6] <> chr(39) + 'betCostWithdraw' + chr(39) then
    begin
      result := false;
  end;
end;

function TArberEvent.IsWeb(const aList : TStringList): boolean;
begin
  result := false;
  if aList[12] = chr(39) + 'WEB' + chr(39) then
    begin
      result := true;
  end;
end;

function TArberEvent.IsNotLive(const aList : TStringList): boolean;
begin
  result := true;
  if aList.Count < 10 then
    begin
    result := false;
    exit;
  end;
  if aList[9] <> '0' then
    begin
      result := false;
  end;
end;

function TArberEvent.IsDeposit(const aList : TStringList): boolean;
begin
  result := false;
  if (Copy(aList[6], 2, 7) = 'deposit') and (Copy(aList[6], Length(aList[6]) - 3, 3) <> 'API') and (Copy(aList[6], Length(aList[6]) - 3, 3) <> 'Api')  then
    begin
      result := true;
  end;
end;

function TArberEvent.IsDepositIncrease(const aList : TStringList): boolean;
begin
  result := false;
  if Copy(aList[6], 2, 15) = 'depositIncrease' then
    begin
      result := true;
  end;
end;

function TArberEvent.IsFromCard(const aList : TStringList): boolean;
var
  str :string;
begin
  result := false;
  str := aList[6];
  if Copy(aList[6], Length(aList[6]) - 4, 4) = 'Card'  then
    begin
      result := true;
  end;
end;

function TArberEvent.Check2017Acc(const aList : TStringList): boolean;
begin
  result := true;
  if Copy(aList[3], 2, 4) <> '2017' then
    begin
      result := false;
      exit;
  end;
end;

procedure TArberEvent.PutArberOrNot(const aFilePath : String; tList : tObjectList; const v1,v2,v3 : double);
var
  textAccFile : TextFile;
  tmpAcc      : TStringList;
  str         : String;
  i           : integer;
  tmpAccd     : tArberAccountData;
begin
  AssignFile(textAccFile, aFilePath);
  Reset(textAccFile);
  Readln(textAccFile, str);
  i := 0;
  while (not EOF(textAccFile)) and (i < tList.Count) do
    begin
      Readln(textAccFile, str);
      tmpAcc := GetArberList(str);

      if tmpAcc[4] = chr(39) + 'Arber' + chr(39) then
        begin
          while (StrToFloat(tArberAccountData(tList[i]).fId) < StrToFloat(tmpAcc[0])) and (i <> tList.Count - 1)  do
            inc(i);
          if tArberAccountData(tList[i]).fId = tmpAcc[0] then
            tArberAccountData(tList[i]).fY := v1;
      end;
      if tmpAcc[4] = chr(39) + 'Pre-Arber' + chr(39) then
        begin
          while (StrToFloat(tArberAccountData(tList[i]).fId) < StrToFloat(tmpAcc[0])) and (i <> tList.Count - 1)  do
            inc(i);
          if tArberAccountData(tList[i]).fId = tmpAcc[0] then
            tArberAccountData(tList[i]).fY := v2;
      end;
      if tmpAcc[4] = chr(39) + 'Pre-Arber 2' + chr(39) then
        begin
          while (StrToFloat(tArberAccountData(tList[i]).fId) < StrToFloat(tmpAcc[0])) and (i <> tList.Count - 1)  do
            inc(i);
          if tArberAccountData(tList[i]).fId = tmpAcc[0] then
            tArberAccountData(tList[i]).fY := v3;
      end;
      tmpAcc.Free;
  end;
end;

function TArberEvent.PutfX2List(const aFilePath : String): TObjectList;
var
  textAccFile : TextFile;
  tmpAcc      : TStringList;
  str         : String;
begin
  AssignFile(textAccFile, aFilePath);
  Reset(textAccFile);
  result := TObjectList.Create;
  while (not EOF(textAccFile) and (result.Count < 200)) do
    begin
      Readln(textAccFile, str);
      tmpAcc := GetArberList(str);
      if tmpAcc.Count > 5 then
        begin
          if CheckValidAcc(tmpAcc) and Check2017Acc(tmpAcc) and IsBet(tmpAcc) then
            result.Add(tmpAcc)
          else
            tmpAcc.Free;
      end
      else  tmpAcc.Free;
  end;
end;


function TArberEvent.getDayOfYear(const str : string): integer;
var
  d1   : TDateTime;
  str1 : string;
  format : TFormatSettings;
begin
  format.DateSeparator := '-';
  format.TimeSeparator := ':';
  format.ShortDateFormat := 'yyyy-mm-dd';
  format.ShortTimeFormat := 'hh24:mi:ss';
  str1 := Copy(str, 2, Length(str) - 2);
  d1 := StrToDateTime(str1, format);
  result := DayOfTheYear(d1);
end;

function TArberEvent.CountfX5(const aFilePath : String): double;
var
  textBetFile : TextFile;
  tmpAcc      : TStringList;
  str         : String;
  count       : integer;
  tmp         : double;
begin
  AssignFile(textBetFile, aFilePath);
  Reset(textBetFile);
  result := 0;
  count := 0;
  while (not EOF(textBetFile)) do
    begin
      Readln(textBetFile, str);
      tmpAcc := GetArberList(str);
      if tmpAcc.Count > 6 then
        begin
          if (IsSingleBet(tmpAcc)) and (TryStrToFloat(tmpAcc[tmpAcc.Count - 1], tmp)) then
            begin
              if tmp > 1 then
                begin
                  result := result + tmp;
                  Inc(count);
              end;
          end;
      end;
      tmpAcc.Free;
  end;
  if count <> 0 then
    result := result / count;
end;

function TArberEvent.CountfX7(const aFilePath : String): double;
var
  textBetFile : TextFile;
  tmpAcc, tmpSport : TStringList;
  countSports : TList<String>;
  str         : String;
  tmp         : double;
  count       : integer;
  curDate, tmpDate : integer;
  max, maxCount : integer;
begin
  AssignFile(textBetFile, aFilePath);
  Reset(textBetFile);
  result := 0;
  count := 0;
  max := 0;
  maxCount := 0;
  curDate := 0;
  countSports := TList<String>.Create;
  while (not EOF(textBetFile)) do
    begin
      Readln(textBetFile, str);
      tmpAcc := GetArberList(str);
      if tmpAcc.Count > 6 then
        begin
          if Length(tmpAcc[2])>15 then
            begin
              tmpDate := getDayOfYear(tmpAcc[2]);
              if tmpDate <> curDate then
                begin
                  if curDate <> 0 then
                    begin
                      if max < countSports.Count then
                        begin
                          max := countSports.Count;
                          maxCount := count;
                      end;
                      countSports.Clear;
                  end;
                  curDate := tmpDate;
                  count := 1;
                  countSports.Add(tmpAcc[6]);
              end
              else
                begin
                  if countSports.IndexOf(tmpAcc[6]) = -1 then
                    countSports.Add(tmpAcc[6]);
                  inc(count);
              end;
          end;
      end;
      tmpAcc.Free;
  end;
  if max < countSports.Count then
    begin
      max := countSports.Count;
      maxCount := count;
  end;
  if maxCount <> 0 then
    result := max / Sqrt(maxCount);
end;

function TArberEvent.CountfX8(const aFilePath : String): double;
var
  textBetFile : TextFile;
  tmpAcc      : TStringList;
  str         : String;
  web, count  : integer;
begin
  AssignFile(textBetFile, aFilePath);
  Reset(textBetFile);
  result := 1;
  count := 0;
  web := 0;
  while (not EOF(textBetFile)) do
    begin
      Readln(textBetFile, str);
      tmpAcc := GetArberList(str);
      if tmpAcc.Count > 12 then
        begin
          if not IsWeb(tmpAcc) then
            inc(web);
          inc(count);
      end;
      tmpAcc.Free;
  end;
  if count <> 0 then
    result := web / count;

end;

function TArberEvent.CountFX6(const aFilePath : String): integer;
var
  textAccFile : TextFile;
  tmpAcc      : TStringList;
  str         : String;
  dep         : integer;
begin
  AssignFile(textAccFile, aFilePath);
  Reset(textAccFile);
  result := 0;
  dep := 0;
  while (not EOF(textAccFile)) do
    begin
      Readln(textAccFile, str);
      tmpAcc := GetArberList(str);
      if CheckValidAcc(tmpAcc) and IsDeposit(tmpAcc) then
        if not(IsFromCard(tmpAcc)) then
          Inc(dep);
      tmpAcc.Free;
  end;
  result := dep;
end;

function TArberEvent.CountFX9(const aFilePath : String): double;
var
  textAccFile : TextFile;
  tmpAcc      : TStringList;
  str         : String;
  depInc,dep  : integer;
begin
  AssignFile(textAccFile, aFilePath);
  Reset(textAccFile);
  result := 1;
  dep := 0;
  depInc := 0;
  while (not EOF(textAccFile)) do
    begin
      Readln(textAccFile, str);
      tmpAcc := GetArberList(str);
      if CheckValidAcc(tmpAcc) and IsDeposit(tmpAcc) then
        begin
        if IsDepositIncrease(tmpAcc) then
          begin
            if StrToFloat(tmpAcc[7]) > 0.9 * StrToFloat(tmpAcc[8]) then
              inc(depInc);
            Inc(dep);
        end;
      end;
      tmpAcc.Free;
  end;
  if dep <> 0 then
    result := depInc / dep;
end;

function TArberEvent.GetDep(const aFilePath : String): integer;
var
  textAccFile : TextFile;
  tmpAcc      : TStringList;
  str         : String;
  dep         : integer;
begin
  AssignFile(textAccFile, aFilePath);
  Reset(textAccFile);
  result := 0;
  dep := 0;
  while (not EOF(textAccFile)) do
    begin
      Readln(textAccFile, str);
      tmpAcc := GetArberList(str);
      if CheckValidAcc(tmpAcc) and IsDeposit(tmpAcc) then
        Inc(dep);
      tmpAcc.Free;
  end;
  result := dep;
end;

function TArberEvent.GetBets(const aFilePath : String): integer;
var
  textAccFile : TextFile;
  tmpAcc      : TStringList;
  str         : String;
  bet         : integer;
begin
  AssignFile(textAccFile, aFilePath);
  Reset(textAccFile);
  result := 0;
  bet := 0;
  while (not EOF(textAccFile)) do
    begin
      Readln(textAccFile, str);
      tmpAcc := GetArberList(str);
      if CheckValidAcc(tmpAcc) and IsBet(tmpAcc) then
        Inc(bet);
      tmpAcc.Free;
  end;
  result := bet;
end;


procedure TArberEvent.CountAndWritefX2(const aArberAllAccsList : TObjectList);
var
  i, j : integer;
  tempStrList : TStringList;
  tempObjectList : TObjectList;
  tempAbrerAccData : TArberAccountData;
  tmp, sum, ballance : double;
begin
  for i := 0 to aArberAllAccsList.Count - 1 do
    begin
      tempAbrerAccData := TArberAccountData(aArberAllAccsList[i]);
      tempAbrerAccData.fX2 := 0;
      tmp := 0;
      tempObjectList := tempAbrerAccData.fX2List;
      for j := 0 to tempObjectList.Count - 1 do
        begin
          tempStrList := TStringList(tempObjectList[j]);
          sum := Abs(StrToFloat(tempStrList[7]));
          ballance := StrToFloat(tempStrList[8]);
          if sum + ballance <> 0 then
            tmp := tmp + sum / (sum + ballance);
      end;
      if tempObjectList.Count <> 0 then
        tempAbrerAccData.fX2 := tmp / tempObjectList.Count;
      for j := 0 to tempObjectList.Count - 1 do
        begin
          if TStringList(tempObjectList[j]).Count > 0 then
            TStringList(tempObjectList[j]).Free;
      end;
  end;
end;


function TArberEvent.GetData(const aPath : string; aFileNamesList : TStringList): TObjectList;
var
  i, j, k                  : integer;
  tempId                   : string;
  AccFilePath              : String;
  BetFilePath              : String;
  ArberAcc50First2017List  : TObjectList;
  ArberFullDataList        : TObjectList;
  tempAbrerAccData         : TArberAccountData;
  c, d, tmp                : double;
  textF : textFile;
begin
  ArberFullDataList := TObjectList.Create;
  for i := 0 to aFileNamesList.Count - 1 do
    begin
      if Copy(aFileNamesList[i], Length(aFileNamesList[i]) - 7, 4) = 'BETS' then
        begin
          tempAbrerAccData := TArberAccountData(ArberFullDataList[ArberFullDataList.Count - 1]);
          //tempAbrerAccData.fX3 := CountfX3(aPath + aFileNamesList[i]);
          //tempAbrerAccData.fX5 := CountfX5(aPath + aFileNamesList[i]);
          tempAbrerAccData.fX7 := CountfX7(aPath + aFileNamesList[i]);
          tempAbrerAccData.fX8 := CountfX8(aPath + aFileNamesList[i]);
          continue;
      end;

      AccFilePath := aPath + aFileNamesList[i];

      tempId := Copy(aFileNamesList[i], 0, Length(aFileNamesList[i]) - 9);

      ArberAcc50First2017List := PutfX2List(AccFilePath);

      tempAbrerAccData := TArberAccountData.Create(tempId, ArberAcc50First2017List);
      tempAbrerAccData.fDepCount := GetDep(AccFilePath);
      tempAbrerAccData.fBetsCount := GetBets(AccFilePath);
      if (tempAbrerAccData.fDepCount + tempAbrerAccData.fBetsCount) <> 0 then
        tempAbrerAccData.fX4 := tempAbrerAccData.fDepCount / (tempAbrerAccData.fDepCount + tempAbrerAccData.fBetsCount)
      else
        tempAbrerAccData.fX4 := 0;
      if tempAbrerAccData.fDepCount <> 0 then
        begin
          tempAbrerAccData.fX6 := 1 - CountFX6(AccFilePath) / tempAbrerAccData.fDepCount;
      end;
      tempAbrerAccData.fX9 := 1 - CountFX9(AccFilePath);
      ArberFullDataList.Add(tempAbrerAccData);
  end;

  CountAndWritefX2(ArberFullDataList);

  PutArberOrNot('C:\Users\tsuban.d\Desktop\Arber\PUNTERS.csv', ArberFullDataList, 1, 0.7, 0.5);

  DecimalSeparator := ',';
  c := 0;
  AssignFile(textF, 'C:\Users\tsuban.d\Desktop\Arber1.txt');
  ReWrite(textF);
  for i := 0 to ArberFullDataList.Count - 1 do
  begin
    tempAbrerAccData := TArberAccountData(ArberFullDataList[i]);
    Writeln(textF, tempAbrerAccData.fId + ';' + FloatToStr(RoundTo(tempAbrerAccData.fX2, -3))
      + ';' + FloatToStr(RoundTo(tempAbrerAccData.fX4, -3))
      + ';' + FloatToStr(RoundTo(tempAbrerAccData.fX6, -3)) + ';' + FloatToStr(RoundTo(tempAbrerAccData.fX7, -3))
      + ';' + FloatToStr(RoundTo(tempAbrerAccData.fX8, -3)) + ';' + FloatToStr(RoundTo(tempAbrerAccData.fX9,-3))
      + ';' + FloatToStr(RoundTo(tempAbrerAccData.fY, -2)));

  end;
  Close(textF);

  result := ArberFullDataList;
end;

end.
