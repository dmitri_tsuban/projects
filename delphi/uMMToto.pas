unit uMMToto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, Math, StdCtrls, Types, ComCtrls, RegularExpressions,
  Generics.Collections, Generics.Defaults, Contnrs;

type
  TTotoSample     = array[1..15] of byte;
  TCatArray       = array[9..15] of double;

  TTotoOutcomeRec = record
    fCats    : TCatArray;
    procedure Init; overload;
    procedure Init(const aCats : TCatArray); overload;
  end;

type
  TMMTotoBet = class
    fInfo   : TTotoSample;
    fProb   : double;
    fEV     : double;
    fSum    : double;
    fCount  : integer;
    constructor Create(const aByteArr : TTotoSample; aBetSum : double; aCount : integer);
    destructor  Destroy; override;
    function    GetIndividualWinTotoBets() : TObjectList;
  end;

type
  TMMTotoRules = record
    fMatchCount       : integer;
    fMinGuessOutcome  : integer;    
    fPoolCoef         : TCatArray;  
    fMargin           : double;
    procedure Reset;
    procedure Init(const aMatchCount, aMinGuessOutcome  : integer; aPoolCoef : TCatArray; aMargin : double);
  end;

  TTotoOutcomeRecFile = file of TTotoOutcomeRec;

  TTotoInfoFile = class
  private
    fTotoOutcomeRecFile     : TTotoOutcomeRecFile;

    function  CheckIndex(aIndex: integer): boolean;
  public
    constructor Create(aFileName: string);
    destructor  Destroy; override;
    procedure   ReCreate;
    function    GetOutcomeRec(aIndex: integer)  : TTotoOutcomeRec;
    procedure   SetOutcomeRec(aIndex: integer; aValue: TTotoOutcomeRec);
  end;

  TTotoEventMatrix = record
    fInfo         : array[0..14] of array[0..2] of double;
  end;

type
  TMMTotoEvent = class
  public
    fRules           : TMMTotoRules;
    fPool            : double;
    fPrizeFund       : double;
    fJackpot         : double;
    fC3v15           : double;
    fProbabilities   : TTotoEventMatrix;
    fMyBets          : TObjectList;
    fOutBets         : TObjectList;
    fTotoFile        : TTotoInfoFile;
    fTotoSample      : TTotoSample;

    constructor Create(const aRules : TMMTotoRules);
    destructor  Destroy; override;
    procedure   UpdateInfoFile;
    procedure   writeInfoReqProcess(const h :integer);
    procedure   UpdateTotoBetProb(const aTempTotoBet : TMMTotoBet);
    function    ParseHTMLToTable(const aSourcePath : string)  : boolean;
    procedure   addProbabilitiesFromFile(const aFileName : string);
  end;

  procedure SupplementBetsStringList(var betString : string; var stringList : TStringList; currIndex : integer = 2);
  function  GetByteArrayFromFormatedString(const betString : string) : TTotoSample;
  function  createPoolCoef() : TCatArray;

  function  IntegerToSample(aKey: integer): TTotoSample;
  function  SampleToInteger(const aSample : TTotoSample)  : integer; overload;

  procedure InitBitMaskArrays(var aNumbersArray : array of integer; const aNumberOf1 : integer);

var
  bitMaskKN  : array[9..15] of array of integer;

implementation

var
  OUTCOME_NOTATION, DIMENSION: integer;

procedure updateLoseMatchesIndexes(const aChoosenBitMask : TTotoSample; var aLoseMatchesIndexes : array of integer);
var
  i, j : integer;
begin
  i := 0;
  for j := Low(aChoosenBitMask) to High(aChoosenBitMask) do
    begin
      if aChoosenBitMask[j] = 0 then
        begin
          aLoseMatchesIndexes[i] := j;
          inc(i);
        end;
    end;
end;

procedure updateChooseMatchesIndexes(const aCurrentMatches : TTotoSample; aK : integer; var aChoosenBitMask : TTotoSample);
var
  i : integer;
begin
  for i := Low(aChoosenBitMask) to High(aChoosenBitMask) do
    aChoosenBitMask[i] := 0;
  for i := 1 to aK do
    aChoosenBitMask[aCurrentMatches[i]] := 1;
end;

function buildTotoSample(const totoSample, boolIndexes : TTotoSample; aLoseMatchesValue : array of integer; aK : integer) : TMMTotoBet;
var
  i, j        : integer;
  tempSample  : TTotoSample;
begin
  i := 0;
  for j := Low(tempSample) to High(tempSample) do
    begin
      if boolIndexes[j] = 1 then
        tempSample[j] := totoSample[j]
      else
        begin
          tempSample[j] := aLoseMatchesValue[i];
          inc(i);
        end;
    end;
  result := TMMTotoBet.Create(tempSample, 0, aK);
end;

function convertFromDecToBin(const aNumber : integer) : TTotoSample;
var
  i, tempNumb : integer;
begin
  tempNumb := aNumber;
  for i := 0 to 14 do
    begin
      if tempNumb mod 2 = 1 then
        begin
          result[i + 1] := 1;
          tempNumb := (tempNumb - 1) Shr 1;
      end
      else
        begin
          result[i + 1] := 0;
          tempNumb := tempNumb Shr 1;
      end;
  end;

end;

procedure supplementMatchesAndAddToObjectList(const aK, aH :integer; aIndividualTotoSample, aChoosenBitMask : TTotoSample; var aLoseMatchesIndexes, aLoseMatchesValue : array of integer; var aList : TObjectList);
var
  i, ind, j, l    : integer;
  tempTotoSample  : TMMTotoBet;
begin
  if aK <= aH then
  begin
    for i := 0 to 2 do
    begin
      if not(i = aIndividualTotoSample[aLoseMatchesIndexes[aH - aK]]) then
        begin
          aLoseMatchesValue[aH - aK] := i;
          if aH < High(aIndividualTotoSample) - 1 then
            supplementMatchesAndAddToObjectList(aK, aH + 1, aIndividualTotoSample, aChoosenBitMask, aLoseMatchesIndexes, aLoseMatchesValue, aList)
          else
          begin
            tempTotoSample := buildTotoSample(aIndividualTotoSample, aChoosenBitMask, aLoseMatchesValue, aK);
            aList.Add(tempTotoSample);
          end;
        end;
    end;
  end
  else
    supplementMatchesAndAddToObjectList(aK, aH + 1, aIndividualTotoSample, aChoosenBitMask, aLoseMatchesIndexes, aLoseMatchesValue, aList)
end;

function  TMMTotoBet.GetIndividualWinTotoBets() : TObjectList;
var
  i, j, k, l                : integer;
  min, max, currentMatches  : TTotoSample;
  loseMatchesIndexes        : array of integer;
  loseMatchesValue          : array of integer;
  choosenBitMask            : TTotoSample;
  tempTotoBet               : TMMTotoBet;
begin
  result := TObjectList.Create;
  for k := Low(bitMaskKN) to High(bitMaskKN) - 1 do
    begin
      SetLength(loseMatchesIndexes, High(bitMaskKN) - k);
      SetLength(loseMatchesValue, High(bitMaskKN) - k);
      for i := Low(bitMaskKN[k]) to High(bitMaskKN[k]) do
        begin
          choosenBitMask := convertFromDecToBin(bitMaskKN[k, i]);
          updateLoseMatchesIndexes(choosenBitMask, loseMatchesIndexes);
          supplementMatchesAndAddToObjectList(k, Low(bitMaskKN), fInfo, choosenBitMask, loseMatchesIndexes, loseMatchesValue, result);
      end;
  end;
  tempTotoBet := TMMTotoBet.Create(fInfo, 0, High(fInfo));
  result.Add(tempTotoBet);
end;

procedure TMMTotoRules.Init(const aMatchCount, aMinGuessOutcome  : integer;
                      aPoolCoef : TCatArray; aMargin : double);
var
  i : integer;
begin
  fMatchCount       := aMatchCount;
  fMinGuessOutcome  := aMinGuessOutcome;
  for i := Low(aPoolCoef) to High(aPoolCoef) do fPoolCoef[i] := aPoolCoef[i];
  fMargin           := aMargin;
end;

constructor TMMTotoBet.Create(const aByteArr : TTotoSample; aBetSum : double; aCount : integer);
var
  i : integer;
begin
  for i   := Low(TTotoSample) to High(TTotoSample) do fInfo[i] := aByteArr[i];
  fSum    := aBetSum;
  fEV     := 0;
  fProb   := 0;
  fCount  := aCount;
end;

procedure SupplementBetsStringList(var betString : string; var stringList : TStringList; currIndex : integer = 2);
var
  i : integer;
  str1, str2, str3 : string;
  flag : boolean;
begin
  flag := true;
  if (currIndex = 1) then inc(currIndex);

  for i := currIndex to Length(betString) do
    if (betString[i] <> ',') and (betString[i - 1] <> ',') then
    begin
      if (i = Length(betString)) or (betString[i + 1] = ',') then
      begin 
        str1 := Copy(betString, 1, Length(betString));
        Delete(str1, i - 1, 1);
        Delete(betString, i, 1);
        SupplementBetsStringList(str1, stringList, i - 1);
        SupplementBetsStringList(betString, stringList, i - 1);
      end
      else begin 
        str1 := Copy(betString, 1, Length(betString));
        str2 := Copy(betString, 1, Length(betString));
        Delete(str1, i + 1, 1); Delete(str1, i - 1, 1); 
        Delete(str2, i - 1, 2); 
        Delete(betString, i, 2); 
        SupplementBetsStringList(str1, stringList, i - 1);
        SupplementBetsStringList(str2, stringList, i - 1);
        SupplementBetsStringList(betString, stringList, i - 1);
      end;
      flag := false; 
      break;
    end;
  if (flag) then
    stringList.Add(betString);
end;

function GetByteArrayFromFormatedString(const betString : string) : TTotoSample;
var
  tempString : string;
  replaceFlags : TReplaceFlags;
  i : integer;
begin
  replaceFlags := [rfReplaceAll, rfIgnoreCase];
  tempString := StringReplace(betString, ',', '', replaceFlags);
  tempString := StringReplace(tempString, '1', '0', replaceFlags);
  tempString := StringReplace(tempString, 'X', '1', replaceFlags);
  for i := 1 to Length(tempString) do Result[i] := StrToInt(tempString[i]);
end;

procedure TMMTotoEvent.UpdateTotoBetProb(const aTempTotoBet : TMMTotoBet);
var
  i     : integer;
  prob  : double;
begin
  prob := 1;
  for i := 0 to fRules.fMatchCount - 1 do
    prob := prob * fProbabilities.fInfo[i, aTempTotoBet.fInfo[i]];
end;

function TMMTotoEvent.ParseHTMLToTable(const aSourcePath: string) : boolean;
var
  i, j, k, l          : integer;
  currString          : string;
  stringList          : TStringList;
  upperLimit          : integer;
  prob                : double;
  byteArr             : TTotoSample;
  sourceFile          : TextFile;
  currBetStringList   : TStringList;
  betsNumber, cashToOneBet, indexNumber : integer;
  totalStringCash     : double;
  stringArray         : string;
  tempTotoBet         : TMMTotoBet;
  //
begin

  stringList := TStringList.Create;
  stringList.LoadFromFile(aSourcePath);
  upperLimit := stringList.Count;

  Assign(sourceFile, aSourcePath);
  Reset(sourceFile);
  k := 0;
  repeat
    inc(k);
    Readln(sourceFile, currString);
  until (Pos('Содержимое', currString) <> 0);

  stringList.Free;

  for i := k + 1 to upperLimit - 6 do  
  begin
    Readln(sourceFile, indexNumber, totalStringCash, betsNumber, stringArray);
    cashToOneBet := Trunc(totalStringCash / betsNumber);
    stringArray := Copy(stringArray, 3, Length(stringArray) - 3);
    currBetStringList := TStringList.Create;
    SupplementBetsStringList(stringArray, currBetStringList);
    for j := 0 to currBetStringList.Count - 1 do
    begin
      byteArr := GetByteArrayFromFormatedString(currBetStringList[j]);
      tempTotoBet := TMMTotobet.Create(byteArr, cashToOneBet, -1);
      UpdateTotoBetProb(tempTotoBet);
      fOutBets.Add(tempTotoBet);
    end;
    currBetStringList.Free;
    currBetStringList := nil;
  end;
  Close(sourceFile);
  Result := true;
end;

procedure TMMTotoEvent.UpdateInfoFile;
begin
  writeInfoReqProcess(1);
end;

procedure TMMTotoEvent.writeInfoReqProcess(const h : integer);
var
  i, j, l, counter  : integer;
  tempTotoRec       : TTotoOutcomeRec;
  tempTotoBet       : TMMTotoBet;
begin
  for i := 0 to 2 do
    begin
      fTotoSample[h] := i;
      if h < fRules.fMatchCount then
        writeInfoReqProcess(h + 1)
      else
        begin
          tempTotoRec.Init;
          for j := 0 to fOutBets.Count - 1 do
            begin
              counter     := 0;
              tempTotoBet := TMMTotoBet(fOutBets[j]);
              for l := 1 to fRules.fMatchCount do
                begin
                  if fTotoSample[l] = tempTotoBet.fInfo[l] then counter := counter + 1;
              end;
              if counter >= fRules.fMinGuessOutcome then
                tempTotoRec.fCats[counter] := tempTotoRec.fCats[counter] + tempTotoBet.fSum;
          end;
          fTotoFile.SetOutcomeRec(SampleToInteger(fTotoSample), tempTotoRec);
      end;
  end;
end;

function createPoolCoef() : TCatArray;
var
  tempPoolCoef : TCatArray;
begin
  tempPoolCoef[15] := 0.05;
  tempPoolCoef[14] := 0.05;
  tempPoolCoef[13] := 0.05;
  tempPoolCoef[12] := 0.05;
  tempPoolCoef[11] := 0.1;
  tempPoolCoef[10] := 0.2;
  tempPoolCoef[9] := 0.4;
  result := tempPoolCoef;
end;

constructor TMMTotoEvent.Create(const aRules : TMMTotoRules);
var i : integer;
begin
  fC3v15 := Math.Power(3,15);
  fRules := aRules;
  fMyBets   := TObjectList.Create;
  fOutBets  := TObjectList.Create;
end;

destructor TMMTotoEvent.Destroy;
begin
  if Assigned(fMyBets)  then FreeAndNil(fMyBets);
  if Assigned(fOutBets) then FreeAndNil(fOutBets);
end;

function SampleToInteger(const aSample : TTotoSample) : integer;
var
  i, tempPower: integer;
begin
  tempPower := 1;
  Result    := 0;
  for i := High(aSample) downto Low(aSample) do begin
    System.Inc(Result, aSample[i] * tempPower);
    tempPower := tempPower * OUTCOME_NOTATION;
  end;
  result := result;
end;

function IntegerToSample(aKey: integer): TTotoSample;
var
  i, tempPower: integer;
begin
  tempPower := Round(Power(OUTCOME_NOTATION, DIMENSION - 1));
  for i := Low(Result) to High(Result) do begin
    Result[i] := aKey div tempPower;
    aKey := aKey - tempPower * Result[i];
    tempPower := tempPower div OUTCOME_NOTATION;
  end;
end;

{ TTestRec }

procedure TTotoOutcomeRec.Init;
var
  i : integer;
begin
  for i := Low(fCats) to High(fCats) do fCats[i] := 0;
end;

procedure TTotoOutcomeRec.Init(const aCats : TCatArray);
var
  i : integer;
begin
  for i := Low(aCats) to High(aCats) do fCats[i] := aCats[i];
end;

{ TTotoInfoFile }

function TTotoInfoFile.CheckIndex(aIndex: integer): boolean;
const
  UPPER_BORDER = 14348907 - 1; // 3^15 - 1
begin
  Result := (aIndex >= 0) and (aIndex <= UPPER_BORDER);
end;

constructor TTotoInfoFile.Create(aFileName: string);
begin
  Assign(fTotoOutcomeRecFile, aFileName);
  if (not FileExists(aFileName)) then begin
    Rewrite(fTotoOutcomeRecFile);
    Recreate;
  end else
    Reset(fTotoOutcomeRecFile);
end;

destructor TTotoInfoFile.Destroy;
begin
  CloseFile(fTotoOutcomeRecFile);
  inherited;
end;

function TTotoInfoFile.GetOutcomeRec(aIndex: integer): TTotoOutcomeRec;
begin
  Assert(CheckIndex(aIndex));
  Seek(fTotoOutcomeRecFile, aIndex);
  Read(fTotoOutcomeRecFile, Result);
end;

function fuct(const n : integer): int64;
begin
  if n <= 1 then
    result := 1
  else
    result := n * fuct(n - 1);
end;

function C(const k, n : integer): integer;
begin
  result := Trunc(fuct(n) / (fuct(n - k) * fuct(k)));
end;

procedure TTotoInfoFile.ReCreate;
var
  i, upperBound : integer;
  rec           : TTotoOutcomeRec;
begin
  CloseFile(fTotoOutcomeRecFile);
  Rewrite(fTotoOutcomeRecFile);
  Seek(fTotoOutcomeRecFile, 0);
  upperBound := Round(Power(3, 15)) - 1;
  for i := 0 to upperBound do Write(fTotoOutcomeRecFile, rec);
  CloseFile(fTotoOutcomeRecFile);
  Reset(fTotoOutcomeRecFile);
end;

procedure TTotoInfoFile.SetOutcomeRec(aIndex: integer; aValue: TTotoOutcomeRec);
begin
  Assert(CheckIndex(aIndex));
  Seek(fTotoOutcomeRecFile, aIndex);
  Write(fTotoOutcomeRecFile, aValue);
end;

procedure TMMTotoEvent.addProbabilitiesFromFile(const aFileName : string);
var
  i, index    : integer;
  reg         : TRegEx;
  M           : TMatchCollection;
  tempFile    : TextFile;
  str         : string[50];
  tempDouble  : double;
begin
  index := 0;
  AssignFile(tempFile, aFileName);
  Reset(tempFile);
  reg := TRegEx.Create('\d{1,2}.\d{2}');
  while ((not EOF(tempFile)) and (index < 45)) do 
    begin
      Readln(tempFile, str);
      M:=reg.Matches(str);
      for  i := 0 to M.Count - 1 do
        begin
          str := Copy(M.Item[i].Value,1,5);
          tempDouble := StrToFloat(str);
          fProbabilities.fInfo[trunc(index / 3), index mod 3] := tempDouble / 100;
          index := index + 1;
      end;
  end;
  Close(tempFile);
end;

destructor TMMTotoBet.Destroy;
begin
  inherited;
end;

procedure InitBitMaskArrays(var aNumbersArray : array of integer; const aNumberOf1 : integer);
var
  count            : integer;
  BScount, i, j, p : integer;
begin
  BScount := 0;
  for i := Trunc(Power(2, aNumberOf1)) - 1 to Trunc(Power(2, 15)) - 1 do
    begin
      count   := 0;
      P       := 1;
      for j := 1 to 15 do
        begin
          if (i AND P = P) then inc(count);
          p := p * 2;
      end;
      if count = aNumberOf1 then
        begin
          aNumbersArray[BSCount] := i;
          inc(BSCount);
      end;
  end;
end;

procedure TMMTotoRules.Reset;
var
  i : integer;
begin
  fMatchCount       := 0;
  fMinGuessOutcome  := 0;
  fMargin           := 0;
  for i := Low(fPoolCoef) to High(fPoolCoef) do fPoolCoef[i] := 0;
end;
var
  i : integer;
initialization
  for i := Low(bitMaskKN) to High(bitMaskKN) do
    begin
      SetLength(bitMaskKN[i], C(i, 15));
      InitBitMaskArrays(bitMaskKN[i], i);
  end;
  DIMENSION         := High(TTotoSample) - Low(TTotoSample) + 1;
  OUTCOME_NOTATION  := 3;
end.
